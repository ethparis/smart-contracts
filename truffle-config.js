/*
 * This truffle script will deploy your smart contracts to your new S-Chain.
 *
 *  @param {String} mnemonic - Provide your MetaMask seed words.
 *  @param {String} privateKey - Provide your Private Key.
 *  @param {String} provider - Provide your SKALE endpoint address.
 */

// HDWalletProvider uses MetaMask to sign the smart contract deployment transaction
let HDWalletProvider = require("truffle-hdwallet-provider");
let mnemonic = "dove cream immense private maze rely poet push lock skate review cotton";

module.exports = {
    networks: {
        hd_wallet: {
            provider: () => new HDWalletProvider(mnemonic, "http://104.248.242.35:8003/"),
            gasPrice: 0,
            network_id: "*"
        }
    },
    // Configure your compilers
    compilers: {
      solc: {
        version: "0.4.25",    // Fetch exact version from solc-bin (default: truffle's version)
        docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
        settings: {          // See the solidity docs for advice about optimization and evmVersion
         optimizer: {
           enabled: false,
           runs: 200
         },
         evmVersion: "byzantium"
        }
      }
    }
}
