pragma solidity ^0.4.25;

import "./BigNumber.sol";
pragma experimental ABIEncoderV2;

contract Ballot {

    using BigNumber for *;

    uint constant BN_SIZE = 2048;

    uint public endTimestamp;
    BigNumber.instance[] public primes;
    BigNumber.instance[] primes_inv;
    BigNumber.instance public aggregate;
    BigNumber.instance public modulus;
    BigNumber.instance public generator;
    BigNumber.instance public public_key;

    event CheckNIPOKEKEvent(bytes c1, bytes a, bytes e, bytes z, bool result);
    event NewVoteCast(address indexed, bytes);
    event DebugNIDZKPEPloop(uint index, bytes left, bytes right);

    constructor(uint _endTimestamp, bytes[] _primes, bytes[] _primes_inv, bytes memory _modulus, bytes memory _generator, bytes memory _public_key) public {
        require(_primes.length == _primes_inv.length);
        endTimestamp = _endTimestamp;
        BigNumber.instance memory computed_inv;
        BigNumber.instance memory p;
        BigNumber.instance memory p_inv;
        for(uint i = 0; i < _primes.length; i++) {
            p = getBigNumber(_primes[i]);
            p_inv = getBigNumber(_primes_inv[i]);
            computed_inv = p.mod_inverse(modulus, p_inv);
            require(BigNumber.cmp(computed_inv, p_inv, false) == 0);
            primes.push(p);
            primes_inv.push(p_inv);
        }
        aggregate = BigNumber.instance({val: hex"0000000000000000000000000000000000000000000000000000000000000001", neg: false, bitlen: 1});
        modulus = BigNumber.instance(_modulus, false, BN_SIZE);
        generator = BigNumber.instance(_generator, false, BN_SIZE);
        public_key = BigNumber.instance(_public_key, false, BN_SIZE);
    }

    function castVote(
        bytes prime_c1,
        bytes prime_c2,
        bytes commitment,
        bytes challenge,
        bytes response)
        external
    {
        require(now < endTimestamp);
        require(checkNIPOKEK(prime_c1, commitment, challenge, response));
        updateAggregate(prime_c2);
        emit NewVoteCast(msg.sender, aggregate.val);
    }

    function updateAggregate(bytes memory prime_c2) private {
        BigNumber.instance memory bn_prime_c2 = getBigNumber(prime_c2);
        aggregate = bn_prime_c2.modmul(aggregate, modulus);
    }

    function checkNIPOKEK(
        bytes memory prime_c1,
        bytes memory commitment,
        bytes memory challenge,
        bytes memory response
    ) private view returns(bool ret)
    {
        BigNumber.instance memory c1 = getBigNumber(prime_c1);
        BigNumber.instance memory a = getBigNumber(commitment);
        BigNumber.instance memory e = getBigNumber(challenge);
        BigNumber.instance memory z = getBigNumber(response);

        BigNumber.instance memory gz = generator.prepare_modexp(z, modulus);
        BigNumber.instance memory c1e = c1.prepare_modexp(e, modulus);
        c1e = c1e.modmul(a, modulus);
        ret = (BigNumber.cmp(gz, c1e, false) == 0);
        emit CheckNIPOKEKEvent(prime_c1, commitment, challenge, response, ret);
    }

    function checkNIDZKPEP(
        bytes memory prime_c2,
        bytes[] memory commitments,
        bytes[] memory challenges,
        bytes[] memory responses
    ) private view returns(bool ret)
    {
        require(primes.length == commitments.length);
        require(challenges.length == commitments.length);
        require(responses.length == commitments.length);

        BigNumber.instance memory c2 = getBigNumber(prime_c2);
        BigNumber.instance memory a;
        BigNumber.instance memory e;
        BigNumber.instance memory z;
        BigNumber.instance memory left;
        BigNumber.instance memory right;
        bytes memory zero = hex"0000000000000000000000000000000000000000000000000000000000000000";
        BigNumber.instance memory sum_e = getBigNumber(zero);
        BigNumber.instance memory pk = public_key;
        BigNumber.instance memory mod = modulus;
        bytes[] memory hash_a;
        ret = true;

        for(uint i = 0; i < commitments.length; i++) {
            a = getBigNumber(commitments[i]);
            e = getBigNumber(challenges[i]);
            z = getBigNumber(responses[i]);
            left = pk.prepare_modexp(z, mod);
            right = c2.modmul(primes_inv[i], mod);
            right = right.prepare_modexp(e, mod);
            right = right.modmul(a, mod);
            emit DebugNIDZKPEPloop(i, left.val, right.val);
            ret = ret && BigNumber.cmp(left, right, false) == 0;
        }
    }

    function getBigNumber(bytes memory data) private view returns(BigNumber.instance memory bn) {
        bn = BigNumber._new(data, false, false);
    }
}
