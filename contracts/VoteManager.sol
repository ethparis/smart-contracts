pragma solidity ^0.4.25;
pragma experimental ABIEncoderV2;

import "./Ballot.sol";

contract VoteManager {

    struct Campaign {
        uint ID;
        string title;
        Ballot ballot;
        string[] items;
        bytes primes;
    }

    event NewBallot(uint indexed ID, string title);

    uint public lastID;
    mapping(uint => Campaign) public campaigns;

    function createCampaign(
        string _title,
        uint _minutes,
        string[] memory _items,
        bytes memory _primes,
        bytes memory _primes_inv,
        bytes memory modulus,
        bytes memory generator,
        bytes memory public_key
    )
        public returns(uint)
    {
        //require(_items.length >= 2);
        //require(_items.length == _primes.length);
        require(_minutes > 1);
        bytes[] memory prime_array = unpackPrimes(_primes, 32);
        uint inv_size = _primes_inv.length /prime_array.length;
        bytes[] memory prime_inv_array = unpackPrimes(_primes_inv, inv_size);
        Ballot newBallot = new Ballot(now + (_minutes * 1 minutes), prime_array, prime_inv_array, modulus, generator, public_key);
        ++lastID;
        campaigns[lastID] = Campaign(lastID, _title, newBallot, _items, _primes);
        emit NewBallot(lastID, _title);
        return lastID;
    }

    function getChoices(uint ID) public view returns(string[], bytes) {
        Campaign memory campaign = campaigns[ID];
        return(campaign.items, campaign.primes);
    }

    function unpackPrimes(bytes memory _bytes, uint size) internal pure returns(bytes[] memory) {
        bytes[] memory ret = new bytes[](_bytes.length / size);
        uint j;
        for(uint i = 0; i < _bytes.length; i += size) {
            ret[j] = slice(_bytes, i, size);
            ++j;
        }
        return ret;
    }

    function slice(
           bytes memory _bytes,
           uint _start,
           uint _length
       )
           internal
           pure
           returns (bytes memory)
       {
           require(_bytes.length >= (_start + _length));

           bytes memory tempBytes;

           assembly {
               switch iszero(_length)
               case 0 {
                   // Get a location of some free memory and store it in tempBytes as
                   // Solidity does for memory variables.
                   tempBytes := mload(0x40)

                   // The first word of the slice result is potentially a partial
                   // word read from the original array. To read it, we calculate
                   // the length of that partial word and start copying that many
                   // bytes into the array. The first word we copy will start with
                   // data we don't care about, but the last `lengthmod` bytes will
                   // land at the beginning of the contents of the new array. When
                   // we're done copying, we overwrite the full first word with
                   // the actual length of the slice.
                   let lengthmod := and(_length, 31)

                   // The multiplication in the next line is necessary
                   // because when slicing multiples of 32 bytes (lengthmod == 0)
                   // the following copy loop was copying the origin's length
                   // and then ending prematurely not copying everything it should.
                   let mc := add(add(tempBytes, lengthmod), mul(0x20, iszero(lengthmod)))
                   let end := add(mc, _length)

                   for {
                       // The multiplication in the next line has the same exact purpose
                       // as the one above.
                       let cc := add(add(add(_bytes, lengthmod), mul(0x20, iszero(lengthmod))), _start)
                   } lt(mc, end) {
                       mc := add(mc, 0x20)
                       cc := add(cc, 0x20)
                   } {
                       mstore(mc, mload(cc))
                   }

                   mstore(tempBytes, _length)

                   //update free-memory pointer
                   //allocating the array padded to 32 bytes like the compiler does now
                   mstore(0x40, and(add(mc, 31), not(31)))
               }
               //if we want a zero-length slice let's just return a zero-length array
               default {
                   tempBytes := mload(0x40)

                   mstore(0x40, add(tempBytes, 0x20))
               }
           }

           return tempBytes;
       }

}
